from multiprocessing import Lock

COLORS = {"red" : "\u001b[31m" ,
          "green" : "\u001b[32m" ,
          "blue" : "\u001b[34m" ,
          "yellow" : "\u001b[33m",
          "purple" : "\u001b[35m",
          "cyan" : "\u001b[36m",
          "reset" : "\u001b[m"}

class Card:
    def __init__(self, number, color):
        self.number = number
        self.color = color  

    def __repr__(self):
        return f"{COLORS[self.color]}{self.number}{COLORS['reset']}"

class Memory:
    def __init__(self) :
        self.info_token = 3
        self.fuse_token = 3
        self.suits = []
        self.lock = Lock()


    def read_memory(self):
        with self.lock:
            res = {"info" : self.info_token , "fuse" : self.fuse_token , "suits" : self.suits}
        return res

    def set_up_mem(self,num_players):
        with self.lock:
            self.suits = [[] for i in range(num_players)] 
            self.info_token += num_players


    def play_card(self,card):
        with self.lock:
            for i in range(len(self.suits)):
                if len(self.suits[i]) == 0 or card.color == self.suits[i][-1].color :
                    if card.number == 1 and len(self.suits[i]) == 0:
                        self.suits[i].append(card)
                        res = True
                    elif len(self.suits[i]) > 0 and card.number == self.suits[i][-1].number + 1:
                        self.suits[i].append(card)
                        res = True
                        if card.number == 5 :
                            self.info_token +=1
                    else :
                        self.fuse_token -= 1
                        res = False
                    break
        return res
    
    def use_info_token(self):
        with self.lock:
            if self.info_token > 0 :
                self.info_token -= 1
                res = True
            else:
                res = False
        return res
    
    def is_game_over(self):
        res = False
        status = "playing"
        
        if self.fuse_token == 0:
            res = True 
            status = "defeat"
        else :
            counter = 0
            for suit in self.suits:
                if len(suit) == 5 :
                    counter +=1
            if  counter == len(self.suits) :
                res = True
                status = "victory"
        return res , status
    
    def go_to_end(self,num_players):
        for i in range(num_players):
            for j in range(1,5):
                self.play_card(Card(j,list(COLORS.keys())[i]))



