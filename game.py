import sys
import socket
from random import shuffle
from multiprocessing.managers import BaseManager
from multiprocessing import Process
from threading import Thread ,Event
import pickle
import signal
import os
from shared_memory import Memory, Card

COLORS = {"red" : "\u001b[31m" ,
          "green" : "\u001b[32m" ,
          "blue" : "\u001b[34m" ,
          "yellow" : "\u001b[33m",
          "purple" : "\u001b[35m",
          "cyan" : "\u001b[36m",
          "reset" : "\u001b[m"}

MAX_NUM_PLAYERS = len(COLORS)-1

HOST = "localhost"
PORT = 6666

def wait_turn(my_turn):
    my_turn.wait()
    my_turn.clear()
    
def client(client_socket,num_players,shared_memory,id,my_turn,end_turn,end_of_game,finish, mode):
    global deck

    with client_socket:
        data = client_socket.recv(1024)
        pid = int(data.decode())
        client_socket.sendall(str(id+1).encode())
        data = client_socket.recv(1024)
        if  data.decode() == "hand" :
            client_socket.sendall(pickle.dumps(([deck.pop() for i in range(5)],num_players,mode)))  

        game_over = False
        status = "playing"

        while not game_over :
            wait_turn(my_turn)
            if not end_of_game.is_set() :
                m = "my turn"
                client_socket.sendall(m.encode())
            
                data = client_socket.recv(1024).decode()
                while data != "end" :
                    if data == "draw" :
                        client_socket.sendall(pickle.dumps(deck.pop()))
                    elif data == "game state":
                        client_socket.sendall(pickle.dumps(shared_memory.read_memory()))
                    elif data == "play card" :
                        m ="ok"
                        client_socket.sendall(m.encode())
                        card = pickle.loads(client_socket.recv(4096))
                        res = shared_memory.play_card(card)
                        client_socket.sendall(str(res).encode())
                    elif data == "info" :
                        res = shared_memory.use_info_token()
                        client_socket.sendall(str(res).encode())

                    data = client_socket.recv(1024).decode()
            
            game_over, status = shared_memory.is_game_over()
                
            if not game_over:
                end_turn.set()

    end_of_game.set()
 
    if status == "victory" :
        os.kill(pid,signal.SIGUSR1)   
    else :
        os.kill(pid,signal.SIGUSR2)   

    client_socket.close()
    finish.set()
    end_turn.set()
       
def all_threads_finished(finishes):
    counter = 0
    for thead in finishes :
        if thead.is_set():
            counter +=1

    return counter == len(finishes)

def generate_deck(num_players):
    deck = []
    for i in range(num_players):
        color = list(COLORS.keys())[i]
        deck += [Card(1,color)]*3 + [Card(2,color)]*2 + [Card(3,color)]*2 + [Card(4,color)]*2 + [Card(5,color)]
    shuffle(deck)

    return deck

def create_server():
    global server_socket, PORT
    online = False
    while not online:
        try :
            server_socket.bind((HOST, PORT))
            online = True
        except:
            PORT += 1

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print("required number of players argument missing, terminating.", file=sys.stderr)
        sys.exit(1)
        
    try:
        n_players = int(sys.argv[1])
    except ValueError:
        print(f"bad number of players argument: {sys.argv[1]}, terminating.", file=sys.stderr)
        sys.exit(2)
        
    if n_players < 2 or n_players > MAX_NUM_PLAYERS:
        print(f"invalid number of players : {n_players}, must be between 2 and {MAX_NUM_PLAYERS}, terminating.", file=sys.stderr)
        sys.exit(3)

    mode = "normal"

    if len(sys.argv) > 2 :
        mode = sys.argv[2]
        
        if mode != "cheat" :
            mode = "normal"

    deck = generate_deck(n_players)

    if mode == "cheat":
        for i in range(n_players) :
            deck.append(Card(5,list(COLORS.keys())[i]))

    class GameManager(BaseManager): pass
    GameManager.register("shared_memory",Memory)

    with GameManager() as manager :
        shared_mem = manager.shared_memory()
        shared_mem.set_up_mem(n_players)
        if mode == "cheat":
            shared_mem.go_to_end(n_players)

        turns = [Event() for i in range(n_players)]
        end_turn = Event()
        end_of_game = Event()
        finishes = [Event() for i in range(n_players)]

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket :
            create_server()
            server_socket.listen(n_players)
            proc_clients = []
            i =0
            print(f"awaiting {n_players} players ...")
            while i < n_players :
                client_socket, address = server_socket.accept()
                proc_clients.append(Thread(target=client, args=(client_socket, n_players, shared_mem, i, turns[i], end_turn, end_of_game, finishes[i], mode))) 
                proc_clients[i].start()
                i += 1
                print(f"{i} players connected waiting for {n_players-i} more ...")

        print("\n#########################\nGame Started\n##########################")
        j = 0
        while not all_threads_finished(finishes) :
            turns[j].set()
            end_turn.wait()
            end_turn.clear()
            j = (j+1) % n_players
            
        print("\n#########################\nGame Ended\n##########################")

 
        
    
    

