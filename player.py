import socket
import pickle
import sysv_ipc
import os
import signal

COLORS = {"red" : "\u001b[31m" ,
          "green" : "\u001b[32m" ,
          "blue" : "\u001b[34m" ,
          "yellow" : "\u001b[33m",
          "purple" : "\u001b[35m",
          "cyan" : "\u001b[36m",
          "reset" : "\u001b[m"}

HOST = "localhost"
PORT = 6666
    
##################################
# fonctions Socket    
##################################

def connect_to_server():
    global client_socket, PORT
    online = False
    while not online:
        try :
            client_socket.connect((HOST, PORT))
            online = True
        except:
            PORT += 1

def set_up_player(conn):
    conn.sendall(str(os.getpid()).encode())
    pl_id = int(conn.recv(1024))

    message = "hand"
    conn.sendall(message.encode())
    data = pickle.loads(conn.recv(4096))

    return pl_id ,data[0],data[1],data[2]

def get_game_state(conn):
    message = "game state"
    conn.sendall(message.encode())
    data = conn.recv(4096)
    return pickle.loads(data)

def draw(conn):
    message = "draw"
    conn.sendall(message.encode())
    data = conn.recv(4096)
    return pickle.loads(data)

def wait_turn(conn) :
    my_turn = False
    while not my_turn :
        data = conn.recv(1024)
        my_turn = data.decode() == "my turn" 

def end_turn(conn):
    message = "end"
    conn.sendall(message.encode())

def play_card(card,conn) :
    message = "play card"
    conn.sendall(message.encode())
    data = conn.recv(1024).decode()
    if data == "ok" :
        conn.sendall(pickle.dumps(card))
        res = conn.recv(1024).decode()

    return res == "True"

def use_info_token(conn):
    message = "info"
    conn.sendall(message.encode())
    res = conn.recv(1024).decode()
    return res == "True"

##################################
# fonctions message queue    
##################################

def send_info(dest,itype,value):
    for i in range(1,num_players+1):
        if i != my_player_id:
            mq.send(pickle.dumps({"itype" : itype,"value" : value,"dest" : dest}),type = i)

def send_hand(start = False):
    dest = my_player_id + 1 if start else num_players+1
    for i in range(1,dest):
        if i != my_player_id :
            mq.send(pickle.dumps((my_player_id,hand)),type = i)

def get_message(blocking = False):
    try  :
        message, t = mq.receive(block = blocking,type = my_player_id)
        return message
    except :
        return None

def get_info(info):
    global card_knowledge,info_given

    if info["dest"] == my_player_id :
        if info["itype"] == "color" :
            for i in range(5):
                
                if info["value"] == hand[i].color :
                    card_knowledge[i][0] = info["value"]
        else : 
            for i in range(5):
                
                if info["value"] == str(hand[i].number) :
                    card_knowledge[i][1] = info["value"]
    else :
        info_given.append(f"player {info['dest']} was told {info['value']}")

def empty_queue(blocking = False):
    global players_hands
    i = 0 
    data = get_message(blocking)
    while data is not None :
        message = pickle.loads(data)
        if message.__class__ == tuple:
            players_hands.append(message)
        else :
            get_info(message)
        i += 1
        blocking = i < num_players-1
        data = get_message(blocking)

##################################
# fonctions display 
##################################
def disp_info_given():
    global info_given
    for info in info_given:
        print(info)
    info_given = []

def disp_hand_knowledge():
    print("\nHere is what you know about your hand : ")
    for i in range(len(card_knowledge)):
        print(COLORS[card_knowledge[i][0]]+card_knowledge[i][1],end=" ")
    print(COLORS["reset"]+"\n1 2 3 4 5\n")
    

def disp_hands():
    other_players_hands = players_hands
    for phand in other_players_hands :
        print(f"\nHand of player {phand[0]} : ", end="")
        for card in phand[1]:
            print(card,end=" ")
    print()

def disp_game_state(game_state):
    print(f"\nInformation tokens remaining : {game_state['info']}")
    print(f"Fuse tokens remaining : {game_state['fuse']}")
    for i in range(len(game_state['suits'])):
        print(f"suit {i + 1} : ",end="")
        for card in game_state['suits'][i]:
            print(card,end=" ")
        print()
        
def disp_turn(turn,game_state, mode):
    print(f"#########################\n TURN {turn}\n#########################")

    disp_game_state(game_state)

    disp_info_given()
    
    disp_hands()

    disp_hand_knowledge()
    if mode == "cheat" :
        print(hand)

##################################
# fonctions action
##################################

def play_feedback(good_play):
    if good_play:
        print("Congratulation! The play is valid.")
    else :
        print("Oh no! This is not a valid play.")

def possible_infos(dest):
    pos_colors = []
    pos_nums   = []
    for phand in players_hands :
        if phand[0] == dest:
            for card in phand[1]:
                pos_colors.append(card.color)
                pos_nums.append(str(card.number))
    return pos_colors, pos_nums
            

def info_type(value,dest):
    p_col, p_nums = possible_infos(dest)
    if value in p_col :
        itype = "color" 
    elif value in p_nums  :
        itype = "number"
    else:
        itype = "err"
    return itype

def get_action(conn):
    global players_hands,card_knowledge
    valid_turn = False
    while not valid_turn :
        str_action = input("Enter your action : ")
        action = str_action.split()

        if len(action) == 2 and action[0] == "play" and action[1] in [str(i) for i in range(1,6)] :
            i_card =int(action[1])-1

            play_feedback(play_card(hand[i_card],conn))
            card_knowledge[i_card] = ["reset","?"]
            hand[i_card] = draw(conn) 

            valid_turn = True

        elif len(action) == 3 and action[0] == "tell" and action[1] in [str(i) for i in range(1,num_players+1)] and action[1] != str(my_player_id):
            itype = info_type(action[2],int(action[1]))
            if itype != "err" and use_info_token(conn):
                send_info(int(action[1]),itype,action[2])
                valid_turn = True    
    players_hands = []

##################################
# fonctions signal handling
##################################

def THE_END(sig, frame):
    global client_socket
    print("\n#########################")
    if sig == signal.SIGUSR1 :
        print("Congatulation! You won the game!")
    elif sig == signal.SIGUSR2 :
        print("Sorry, you ran out of fuse token. You lost the game. ")
    elif sig == signal.SIGINT:
        print("The game was interrupted")
    print("#########################")

    if my_player_id == num_players :
        mq.remove()
    client_socket.close()
    exit()
 
signal.signal(signal.SIGUSR1, THE_END)
signal.signal(signal.SIGUSR2, THE_END)
signal.signal(signal.SIGINT, THE_END)

##################################
# MAIN
##################################
if __name__ == "__main__":
    card_knowledge = [["reset","?"],["reset","?"],["reset","?"],["reset","?"],["reset","?"]]
    info_given = []
    players_hands = []

    key = 128
 
    mq = sysv_ipc.MessageQueue(key, sysv_ipc.IPC_CREAT)
 
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    connect_to_server()

    my_player_id , hand, num_players, mode = set_up_player(client_socket)
    counter = 1
    send_hand(True)

    print(f"#########################\n Welcome To Hannabis! \n You are the player {my_player_id}\n#########################")
    if my_player_id == 1 :
        empty_queue(True)

    while True :
        wait_turn(client_socket)

        empty_queue()

        disp_turn(counter,get_game_state(client_socket),mode)

        get_action(client_socket)

        send_hand()

        end_turn(client_socket)

        counter +=1