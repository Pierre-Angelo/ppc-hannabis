# PPC Hannabis

## Description
This is a student project implementing a simplefied version of the game [hannabi](https://en.wikipedia.org/wiki/Hanabi_(card_game)) using various parallel programming tools.

## Installation
The library sysv_ipc is necessary to run the game which only works on Unix-like operating systems.
If the library is not already installed you can download the compressed file [here](http://semanchuk.com/philip/sysv_ipc/). Then uncompress it and issue this command in the root folder :   
**login@hostame:~$ python3 setup.py install --user**

## Usage
To start the game, open a terminal in the /ppc-hannabis folder then issue this command with [num_players] replaced by the number of player desired (between 2 and 6) :  
**user@debian:~/ppc-hannabis$ python3 game.py [num_players]**  

Then open as many terminals as the number of players and issue this command :  
**user@debian:~/ppc-hannabis$ python3 player.py**  
When all players are started, the game starts with player 1.  

If you want to play a card type play and the corresponding number of the card for example if you want to play the first card :  
 **Enter your action : play 1**

Now if you want to tell an information to a player here is what you haveto type :   
 **Enter your action : tell [num_player] [info]**  

for exemple if you want to tell the player 3 what card are green :  
 **Enter your action : tell 3 green**  
and if you want to tell the player 1 what card are 4 :  
 **Enter your action : tell 1 4**  

Finally if you want to see the end quickly, you simply have to add the word "cheat" at the end of the game initializing command. For exemple  :  
**user@debian:~/ppc-hannabis$ python3 game.py 2 cheat**  
